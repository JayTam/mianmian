import request from "@/utils/request";

export function getHotCities() {
  return request({
    url: "/hot_cities",
    method: "get"
  });
}

export function getQuestionData(subject) {
  return request({
    url: "/question/statistics",
    method: "get",
    params: {
      subject
    }
  });
}

export function getDashboardData() {
  return request({
    url: "/dash_board/title",
    method: "get"
  });
}
