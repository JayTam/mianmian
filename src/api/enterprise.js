import request from "@/utils/request";

export function getEnterprises(query) {
  return request({
    url: "/enterprises",
    method: "get",
    params: query
  });
}

export function createEnterprise(enterprise) {
  return request({
    url: "/enterprise",
    method: "post",
    data: {
      enterprise
    }
  });
}

export function updateEnterprise(enterprise) {
  return request({
    url: `/enterprise/${enterprise.id}`,
    method: "put",
    data: {
      enterprise
    }
  });
}

export function deleteEnterprise(id) {
  return request({
    url: `/enterprise/${id}`,
    method: "delete"
  });
}

export function disableEnterprise(id) {
  return request({
    url: `/enterprise/disable/${id}`,
    method: "put"
  });
}
