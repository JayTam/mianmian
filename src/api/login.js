import request from "@/utils/request";

export function authLogin(username, password, code) {
  return request({
    url: "/login",
    method: "post",
    data: {
      username,
      password,
      code
    }
  });
}

export function register(user) {
  return request({
    url: "/register",
    method: "post",
    data: user
  });
}
