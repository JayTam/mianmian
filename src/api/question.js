import request from "@/utils/request";

export function getQuestions(query) {
  return request({
    url: "/questions",
    method: "get",
    params: query
  });
}

export function createQuestion(question) {
  return request({
    url: "/question",
    method: "post",
    data: {
      question
    }
  });
}

export function updateQuestion(question) {
  return request({
    url: `/question/${question.id}`,
    method: "put",
    data: {
      question
    }
  });
}

export function deleteQuestion(id) {
  return request({
    url: `/question/${id}`,
    method: "delete"
  });
}

export function disableQuestion(id) {
  return request({
    url: `/question/disable/${id}`,
    method: "put"
  });
}
