import request from "@/utils/request";

export function getRoles(query) {
  return request({
    url: "/roles",
    method: "get",
    params: query
  });
}

export function createRole(role) {
  return request({
    url: "/role",
    method: "post",
    data: {
      role
    }
  });
}

export function updateRole(role) {
  return request({
    url: `/role/${role.id}`,
    method: "put",
    data: {
      role
    }
  });
}

export function deleteRole(id) {
  return request({
    url: `/role/${id}`,
    method: "delete"
  });
}

export function disableRole(id) {
  return request({
    url: `/role/disable/${id}`,
    method: "put"
  });
}
