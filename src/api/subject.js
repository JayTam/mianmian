import request from "@/utils/request";

export function getSubjects(query) {
  return request({
    url: "/subjects",
    method: "get",
    params: query
  });
}

export function createSubject(subject) {
  return request({
    url: "/subject",
    method: "post",
    data: {
      subject
    }
  });
}

export function updateSubject(subject) {
  return request({
    url: `/subject/${subject.id}`,
    method: "put",
    data: {
      subject
    }
  });
}

export function deleteSubject(id) {
  return request({
    url: `/subject/${id}`,
    method: "delete"
  });
}

export function disableSubject(id) {
  return request({
    url: `/subject/disable/${id}`,
    method: "put"
  });
}
