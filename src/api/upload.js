import request from "@/utils/request";

export function uploadFile() {
  return request({
    url: "/upload",
    method: "post"
  });
}
