import request from "@/utils/request";

export function getUserInfo() {
  return request({
    url: "/user/info",
    method: "get"
  });
}

export function getUserList(query) {
  return request({
    url: "/user/list",
    method: "get",
    params: query
  });
}

export function createUser(user) {
  return request({
    url: "/user",
    method: "post",
    data: {
      user
    }
  });
}

export function updateUser(user) {
  return request({
    url: `/user/${user.id}`,
    method: "put",
    data: {
      user
    }
  });
}

export function deleteUser(id) {
  return request({
    url: `/user/${id}`,
    method: "delete"
  });
}

export function disableUser(id) {
  return request({
    url: `/user/disable/${id}`,
    method: "put"
  });
}
