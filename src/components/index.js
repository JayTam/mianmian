import Vue from "vue";
import MmButton from "./MmButton";
import Pagination from "./pagination/index";

// 全局组件注册
Vue.component("mm-button", MmButton);
Vue.component("Pagination", Pagination);
