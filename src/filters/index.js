import Vue from "vue";

Vue.filter("statusFilter", status => {
  return status ? "启动" : "停用";
});
