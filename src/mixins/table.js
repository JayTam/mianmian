import { Message } from "element-ui";

export default {
  data() {
    return {
      resourceName: "",
      listQuery: {
        page: 1,
        limit: 20,
        total: 0
      },
      listLoading: false,
      tableData: [],
      dialogVisible: false,
      isCreateForm: true,
      form: {
        status: true
      },
      httpRequest: {
        query: null,
        create: null,
        update: null,
        delete: null,
        disable: null
      },
      roleOptions: ["教师", "学员", "管理员"],
      statusOptions: [
        { label: "启动", value: true },
        { label: "停用", value: false }
      ]
    };
  },
  computed: {
    dialogTitle() {
      return this.isCreateForm
        ? `新增${this.resourceName}`
        : `编辑${this.resourceName}`;
    }
  },
  created() {
    this.fetchData();
  },
  methods: {
    async fetchData() {
      this.listLoading = true;
      const { items, pagination } = await this.httpRequest.query(
        this.listQuery
      );
      this.tableData = items;
      this.listQuery.page = pagination.page;
      this.listQuery.total = pagination.total;
      this.listLoading = false;
    },
    handleFilter() {
      this.listQuery.page = 1;
      this.fetchData();
    },
    clearFilter() {
      const whiteKeys = ["page", "limit", "total"];
      for (const key in this.listQuery) {
        if (!whiteKeys.includes(key)) {
          this.listQuery[key] = undefined;
        }
      }
    },
    preSubmitForm() {},
    submitForm() {
      this.preSubmitForm();
      this.$refs.form.validate(async valid => {
        if (valid) {
          if (this.isCreateForm) {
            try {
              await this.httpRequest.create(this.form);
              Message({
                message: `新增${this.resourceName}成功`,
                type: "success"
              });
            } catch (e) {
              Message({
                message: `新增${this.resourceName}失败`,
                type: "error"
              });
            }
          } else {
            try {
              await this.httpRequest.update(this.form);
              Message({
                message: `编辑${this.resourceName}成功`,
                type: "success"
              });
            } catch (e) {
              Message({
                message: `修改${this.resourceName}失败`,
                type: "error"
              });
            }
          }
          this.dialogVisible = false;
          this.fetchData();
        } else {
          return false;
        }
      });
    },
    handleClickCreate() {
      this.isCreateForm = true;
      this.form = {};
      this.dialogVisible = true;
      this.$nextTick(() => {
        this.$refs?.form?.clearValidate();
      });
    },
    handleClickUpdate(instance) {
      this.isCreateForm = false;
      this.form = instance;
      this.dialogVisible = true;
    },
    async handleClickDisable(id) {
      try {
        await this.$confirm(
          `此操作将禁用此${this.resourceName}, 是否继续?`,
          "提示",
          {
            confirmButtonText: "确定",
            cancelButtonText: "取消",
            type: "warning"
          }
        );
        await this.httpRequest.disable(id);
        this.$message({
          type: "success",
          message: `禁用${this.resourceName}成功!`
        });
        this.fetchData();
      } catch (e) {
        this.$message({
          type: "info",
          message: "已取消禁用"
        });
      }
    },
    async handleClickDelete(id) {
      try {
        await this.$confirm(
          `此操作将删除此${this.resourceName}, 是否继续?`,
          "提示",
          {
            confirmButtonText: "确定",
            cancelButtonText: "取消",
            type: "warning"
          }
        );
        await this.httpRequest.delete(id);
        this.$message({
          type: "success",
          message: `删除${this.resourceName}成功!`
        });
        this.fetchData();
      } catch (e) {
        this.$message({
          type: "info",
          message: "已取消删除"
        });
      }
    }
  }
};
