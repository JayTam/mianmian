import Vue from "vue";
import Router from "vue-router";
import routes from "./routes";
import store from "@/store";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import { getToken } from "@/utils/auth";
import { Message } from "element-ui";

// 相关配置，请阅读文档 https://www.npmjs.com/package/nprogress
NProgress.configure({
  trickleRate: 0.1,
  trickleSpeed: 500,
  easing: "ease",
  speed: 800,
  showSpinner: false
});

// 重复跳转相同的路由会报 "NavigationDuplicated" 错误
// 利用原型链修改原本的push方法，过滤掉 "NavigationDuplicated" 错误
const push = Router.prototype.push;
Router.prototype.push = function(location) {
  return push.call(this, location).catch(err => {
    if (err.name === "NavigationDuplicated") {
      return;
    }
    console.error(err);
  });
};

Vue.use(Router);
const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

/*
 * 判断用户是否获取到用户信息，
 * 如果Vuex存储了用户信息，则直接跳转相应页面
 * 如果没有，则拉取用户信息之后再跳转相应页面
 * */
async function getUserInfo(to, from, next) {
  if (!store.getters["user/hasGetUserInfo"]) {
    await store.dispatch("user/getInfo");
  }
  if (
    to.matched.some(record => {
      // 在routes中写了roles = ["admin", "teacher"]才拦截
      // 当没有在路由中定义meta.roles，所有用户都可以访问
      if (!record.meta?.roles) {
        return false;
      }
      return record.meta.roles.includes(store.state.user.role);
    })
  ) {
    next();
  } else {
    Message.error("你没有权限访问该页面");
    await store.dispatch("user/logout");
    await router.replace({ name: "login" });
  }
}

router.beforeEach(async (to, from, next) => {
  NProgress.start();
  const token = getToken();
  // 跳转页面是需要登录的
  if (to.matched.some(record => !record.meta?.notRequireAuth)) {
    if (token) {
      // 用户已经登录
      await getUserInfo(to, from, next);
    } else {
      // 用户还未登录，提示然后跳转到登录页面，并且保存之前页面的路径
      Message({
        message: "需要先登录！",
        type: "warning"
      });
      next({ name: "login", query: { redirect: window.location.pathname } });
    }
  } else {
    // 登录之后，再取访问登录页面，直接跳转到首页
    if (token) {
      if (to.name === "login") {
        next({ name: "dataStatistics" });
      }
      await getUserInfo(to, from, next);
    }
    // 跳转页面不需要登录的，直接跳转
    next();
  }
});

router.afterEach(() => {
  NProgress.done();
});

export default router;
