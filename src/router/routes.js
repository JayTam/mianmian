import Login from "@/views/Login";
import Layout from "@/views/Layout/index";
import User from "@/views/user/UserList";
import UserPermission from "@/views/user/UserPermission";
import EnterpriseList from "@/views/enterprise/EnterpriseList";
import SubjectList from "@/views/subject/SubjectList";
import PostList from "@/views/interview/PostList";

/*
 * title: String 侧边栏标题
 * icon: Icon 侧边栏图标
 * notRequireAuth: Boolean 不需要登录的页面
 * hidden: Boolean 在侧边导航栏不显示
 * */

export default [
  {
    path: "/",
    component: Layout,
    meta: {
      roles: ["admin"],
      title: "数据统计",
      icon: "el-icon-pie-chart"
    },
    children: [
      {
        path: "/",
        name: "dataStatistics",
        component: () => import("@/views/dashboard/index"),
        meta: {
          roles: ["admin"],
          title: "数据概览"
        }
      }
    ]
  },
  {
    path: "/user",
    component: Layout,
    redirect: "/user/list",
    meta: {
      roles: ["admin"],
      title: "用户管理",
      icon: "el-icon-user"
    },
    children: [
      {
        path: "list",
        name: "userList",
        component: User,
        meta: {
          roles: ["admin"],
          title: "用户列表"
        }
      },
      {
        path: "permission",
        name: "userPermission",
        component: UserPermission,
        meta: {
          roles: ["admin"],
          title: "角色管理"
        }
      }
    ]
  },
  {
    path: "/question",
    component: Layout,
    meta: {
      roles: ["admin"],
      title: "题库管理",
      icon: "el-icon-edit-outline"
    },
    children: [
      {
        path: "list",
        name: "questionList",
        component: () => import("@/views/question/QuestionList"),
        meta: {
          roles: ["admin"],
          title: "题库列表"
        }
      }
    ]
  },
  {
    path: "/enterprise",
    component: Layout,
    meta: {
      roles: ["admin"],
      title: "企业管理",
      icon: "el-icon-office-building"
    },
    children: [
      {
        path: "list",
        name: "enterpriseList",
        component: EnterpriseList,
        meta: {
          roles: ["admin"],
          title: "企业列表"
        }
      }
    ]
  },
  {
    path: "/subject",
    component: Layout,
    meta: {
      roles: ["admin"],
      title: "学科管理",
      icon: "el-icon-notebook-2"
    },
    children: [
      {
        path: "list",
        name: "subjectList",
        component: SubjectList,
        meta: {
          roles: ["admin"],
          title: "学科列表"
        }
      }
    ]
  },
  {
    path: "/interview",
    component: Layout,
    meta: {
      roles: ["admin"],
      title: "面试技巧",
      icon: "el-icon-trophy"
    },
    children: [
      {
        path: "list",
        name: "postList",
        component: PostList,
        meta: {
          roles: ["admin"],
          title: "文章列表"
        }
      }
    ]
  },
  {
    path: "/login",
    name: "login",
    hidden: true,
    component: Login,
    meta: {
      notRequireAuth: true
    }
  }
];
