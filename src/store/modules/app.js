const KEY = "SIDE_BAR";

function getCollapseStatus() {
  const status = localStorage.getItem(KEY);
  if (status === null) {
    return false;
  }
  return status !== "false";
}

function setCollapseStatus(status) {
  localStorage.setItem(KEY, status);
}

export default {
  namespaced: true,
  state: {
    // 折叠状态持久化
    sideBarCollapse: getCollapseStatus()
  },
  getters: {},
  mutations: {
    SET_SIDEBAR_STATUS(state, data) {
      state.sideBarCollapse = data;
      setCollapseStatus(state.sideBarCollapse);
    }
  },
  actions: {
    changeSideBarCollapse({ commit, state }) {
      const newStatus = !state.sideBarCollapse;
      commit("SET_SIDEBAR_STATUS", newStatus);
    }
  }
};
