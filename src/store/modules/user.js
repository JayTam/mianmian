import { getUserInfo } from "@/api/user";
import router from "@/router";
import { Message } from "element-ui";
import { removeToken, setToken } from "@/utils/auth";
import { authLogin } from "@/api/login";
import { getUrlParameters } from "@/utils/url";

export default {
  namespaced: true,
  state: {
    id: null,
    name: null,
    email: null,
    role: null,
    origin: null,
    avatar: null,
    createdDate: null,
    remark: null,
    status: null
  },
  getters: {
    hasGetUserInfo: state => state.id !== null
  },
  mutations: {
    SET_ID(state, data) {
      state.id = data;
    },
    SET_NAME(state, data) {
      state.name = data;
    },
    SET_EMAIL(state, data) {
      state.email = data;
    },
    SET_ROLE(state, data) {
      state.role = data;
    },
    SET_ORIGIN(state, data) {
      state.origin = data;
    },
    SET_AVATAR(state, data) {
      state.avatar = data;
    },
    SET_CREATED_DATE(state, data) {
      state.createdDate = data;
    },
    SET_REMARK(state, data) {
      state.remark = data;
    },
    SET_STATUS(state, data) {
      state.status = data;
    }
  },
  actions: {
    async login({ dispatch }, { username, password, code }) {
      try {
        const token = await authLogin(username, password, code);
        setToken(token);
        await dispatch("getInfo");
        const { redirect } = getUrlParameters();
        if (redirect) {
          await router.push({
            path: redirect
          });
        } else {
          await router.push({ name: "dataStatistics" });
        }
        Message({
          message: "登录成功",
          type: "success"
        });
      } catch (e) {
        Message({
          message: `${e}`,
          type: "error"
        });
      }
    },
    async getInfo({ commit, dispatch }) {
      try {
        const {
          id,
          name,
          email,
          role,
          origin,
          avatar,
          createdDate,
          remark,
          status
        } = await getUserInfo();
        commit("SET_ID", id);
        commit("SET_NAME", name);
        commit("SET_EMAIL", email);
        commit("SET_ROLE", role);
        commit("SET_ORIGIN", origin);
        commit("SET_AVATAR", avatar);
        commit("SET_CREATED_DATE", createdDate);
        commit("SET_REMARK", remark);
        commit("SET_STATUS", status);
      } catch (e) {
        await dispatch("logout");
        await router.push({ name: "login" });
        Message({
          message: "获取个人信息有误，请重新登录",
          type: "error"
        });
        console.error(e);
      }
    },
    logout({ commit }) {
      commit("SET_ID", null);
      commit("SET_NAME", null);
      commit("SET_EMAIL", null);
      commit("SET_ROLE", null);
      commit("SET_ORIGIN", null);
      commit("SET_AVATAR", null);
      commit("SET_CREATED_DATE", null);
      commit("SET_REMARK", null);
      commit("SET_STATUS", null);
      removeToken();
      router.push({ name: "login" });
      Message({
        message: "注销成功!",
        type: "success"
      });
    }
  }
};
