export default function(url) {
  const div = document.createElement("div");
  div.setAttribute(
    "style",
    "position:fixed;left:0;top:0;width:100%;height:100%;background:rgba(0,0,0,0.9);z-index:99999"
  );
  const img = document.createElement("img");
  img.setAttribute(
    "style",
    "position:absolute;left:50%;top:50%;transform:translate(-50%,-50%);"
  );
  img.src = url;
  div.appendChild(img);
  document.body.appendChild(div);
  div.addEventListener("click", handle);
  function handle() {
    document.body.removeChild(div);
    div.removeEventListener("click", handle);
  }
}
