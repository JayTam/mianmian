import axios from "axios";
import router from "@/router";
import { getToken } from "@/utils/auth";
import { Message } from "element-ui";
import store from "@/store/index";

const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  timeout: 20000
});

service.interceptors.request.use(
  config => {
    if (getToken()) {
      // 让登陆之后的每个请求都携带token
      config.headers.token = getToken();
    }
    return config;
  },
  error => Promise.reject(error)
);

service.interceptors.response.use(
  async response => {
    const res = response.data;
    // 需要登录的接口，跳转到登录页面，记录当前页面的地址，登录完跳转回当前页
    if (res.code === 401) {
      await store.dispatch("user/logout");
      await router.replace({
        name: "login",
        query: { redirect: window.location.pathname }
      });
      Message({
        message: "需要登录才可访问",
        type: "warning"
      });
      return Promise.reject(`Api ${response.request.responseURL} need login`);
    }
    // code为非200就报错
    if (res.code !== 200) {
      return Promise.reject(res.msg);
    }
    return res.data;
  },
  error => {
    const { code, message } = error;
    if (code === "ECONNABORTED" || message === "Network Error") {
      Message({
        message: "网络有问题，请稍后重试",
        type: "error"
      });
    }
    return Promise.reject(error);
  }
);

export default service;
