export function getUrlParameters() {
  const parameterList = decodeURIComponent(
    window.location.href.split("?")[1]
  ).split("&");
  const parameters = {};
  for (const parameter of parameterList) {
    const [key, value] = parameter.split("=");
    parameters[key] = value;
  }
  return parameters;
}
